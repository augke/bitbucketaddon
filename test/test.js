import { Selector } from 'testcafe'
fixture `Dummy Test`
    .page `https://bitbucket.org/augke/bitbucketaddon/src/master/`

test("Dummy Test Stub", async t => {
    const i = 2;
    const j = 3;
    const res = 5;
    await t
        .expect(res == i + j).ok();
});